# ERS Web Frontend

This project is a web interface for the Emergency Response Service (ERS). It has been built as an [AngularJS][angularjs] application. This project has used the generator tool [Yeoman][yeoman] and the generator [generator-gulp-angular][angular-generator] to produce the initial scaffold and tooling for this project. This generator has set up [Gulp][gulp] to provide developers with all the tasks they will need for development.

## Getting Started

To get you started you can simply clone the repository and install the dependencies.

### Prerequisites

We also use a number of node.js tools to initialize and test. You must have node.js and its package manager (npm) installed.  You can get them from [http://nodejs.org/](http://nodejs.org/).

We also use [Bower][bower] and you need to install this to manage our frontend dependencies.

### Install Dependencies

We have two kinds of dependencies in this project: tools and frontend library / frameworks.  The tools help
us manage and test the application.

* We get the tools we depend upon via `npm`, the [node package manager][npm].
* We get the frontend dependencies via `bower`, a [client-side code package manager][bower].

Running:

```
npm install
```
Installs our tooling dependencies in a folder named `node_modules`. This command will need to be run with root/admin privileges.

```
bower install
```
Installs our frontend dependencies in a folder named `bower_components`.

## Use Gulp tasks

* `gulp` or `gulp build` to build an optimized version of your application in `dist`
* `gulp serve` to launch a browser sync server on your source files
* `gulp serve:dist` to launch a server on your optimized application
* `gulp test` to launch your unit tests with [Karma][karma]
* `gulp test:auto` to launch your unit tests with [Karma][karma] in watch mode
* `gulp protractor` to launch your e2e tests with [Protractor][protractor]
* `gulp protractor:dist` to launch your e2e tests with [Protractor][protractor] on the dist files

## Directory structure

[Best Practice Recommendations for Angular App Structure](https://docs.google.com/document/d/1XXMvReO8-Awi1EZXAXS4PzDzdNvV6pGcuaF4Q9821Es/pub)


[angularjs]: http://angularjs.org/
[less]: http://lesscss.org/
[bower]: http://bower.io
[npm]: https://www.npmjs.org/
[node]: http://nodejs.org
[protractor]: https://github.com/angular/protractor
[karma]: http://karma-runner.github.io
[gulp]: http://gulpjs.com/
[yeoman]: http://yeoman.io/
[angular-generator]: https://github.com/Swiip/generator-gulp-angular
