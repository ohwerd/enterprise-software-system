/* global malarkey:false, toastr:false, moment:false */
(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .constant('ENV', {
      host: 'http://localhost:8080'
    })
    .constant('EmAPI', {
      emergencyEndpoint: '/emergencies'
    })
    ;

})();
