(function() {
  'use strict';

  angular
    .module('emergencyResponseService', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'mgcrea.ngStrap', 'angularMoment']);

})();
