(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .run(runBlock);

  /** @ngInject */
  function runBlock(sessionService) {
    // load in the current user on page refresh or first vist
    var user = sessionService.getCurrentUser();
    sessionService.setCurrentUser(user);
  }

})();
