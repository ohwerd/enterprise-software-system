(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .controller('EmergenciesIndexController', EmergenciesIndexController);

  /** @ngInject */
  function EmergenciesIndexController($scope, $state, $http, Emergency) {

    initController();

    $scope.emergencyClick = function(emergency) {
      $state.go('main.emergencies.view', {emergencyId: emergency.id});
    };

    $scope.closeEmergency = function(emergency) {
      emergency.isOpen = false;
      Emergency.update(emergency, function(e) {}, function(err) {
        // on error, reset
        emergency.isOpen = true;
      });
    };

    $scope.deleteEmergency = function(emergency) {
      Emergency.delete({id: emergency.id}, function() {
        removeEmergencyFromList(emergency);
      });
    };

    $scope.mergeEmergencyClick = function(emergency){
      if ($scope.firstEmMergeSelected) {
        if ($scope.firstEmMergeSelected.id == emergency.id) {
          $scope.firstEmMergeSelected = null;
        } else {
          Emergency.merge({ id1: $scope.firstEmMergeSelected.id, id2: emergency.id }, function() {
            $scope.firstEmMergeSelected = null;
            pollEmergencies();
          });
        }
      } else {
        $scope.firstEmMergeSelected = emergency;
      }
    };

    $scope.isEmergencyMergable = function(emergency) {
      return $scope.firstEmMergeSelected && $scope.firstEmMergeSelected.id != emergency.id;
    }

    function removeEmergencyFromList(emergency) {
      var index = $scope.emergencies.indexOf(emergency);
      $scope.emergencies.splice(index, 1);
    };

    function pollEmergencies() {
      Emergency.query({}, function(emergencies) {
        $scope.emergencies = emergencies;
      });
      $http.get('http://localhost:8080/emergencies', function(resp) {
        console.log(resp);
      }, function(err) {
        console.log(err);
      });
    };

    function initController() {
      $scope.showOpen = true;
      $scope.statusOptions = [
        { label:'Open', value: true },
        { label:'Closed', value: false }
      ];
      pollEmergencies();
    }

  }
})();
