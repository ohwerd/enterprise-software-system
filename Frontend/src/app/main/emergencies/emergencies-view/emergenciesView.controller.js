(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .controller('EmergenciesViewController', EmergenciesViewController);

  /** @ngInject */
  function EmergenciesViewController($scope, $stateParams, Emergency) {
    Emergency.get({id: $stateParams.emergencyId}, function(emergency) {
      $scope.emergency = emergency;
    });
  }
})();
