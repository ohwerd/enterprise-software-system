(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .controller('NoEmergenciesViewController', NoEmergenciesViewController);

  /** @ngInject */
  function NoEmergenciesViewController() {
  }
})();
