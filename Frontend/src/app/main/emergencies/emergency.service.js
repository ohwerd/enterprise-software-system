'use strict';

angular.module('emergencyResponseService')
  .factory('Emergency', function ($resource, ENV, EmAPI) {
    return $resource(ENV.host + EmAPI.emergencyEndpoint + "/:id", {id: '@id'},
    {
        'update' : { method: "PUT" },
        'merge' : {
          method: "POST",
          url: ENV.host + EmAPI.emergencyEndpoint + "/merge/:id1/:id2",
          params: { id1: '@id1', id2: '@id2' }
        }
    });
  });