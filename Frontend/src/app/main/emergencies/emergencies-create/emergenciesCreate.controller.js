(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .controller('EmergenciesCreateController', EmergenciesCreateController);

  /** @ngInject */
  function EmergenciesCreateController($scope, $state, Emergency) {

    $scope.report = function(emergency) {
      Emergency.save(emergency, function(emergency) {
        $state.go('main.report.view', {emergencyId: emergency.id}, {reload: true});
      });
    };

  }
})();
