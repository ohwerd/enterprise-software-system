(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, sessionService) {

    $scope.logout = function() {
      sessionService.logout();
    };

  }
})();
