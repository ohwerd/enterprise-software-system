(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .config(config);

  /** @ngInject */
  function config($provide, $httpProvider) {

    $provide.factory('unauthorisedInterceptor', function($q, $injector) {
      return {
       'responseError': function(rejection) {
          var sessionService = $injector.get('sessionService');
          if (rejection.status == 401) {
            sessionService.setCurrentUser(null);
          }
          return $q.reject(rejection);
        }
      };
    });

    $httpProvider.interceptors.push('unauthorisedInterceptor');
  }

})();
