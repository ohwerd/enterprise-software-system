(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($scope, $state, sessionService) {

    $scope.login = function() {
      sessionService.login($scope.username, $scope.password);
    };

  }
})();
