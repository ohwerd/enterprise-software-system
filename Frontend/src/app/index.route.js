(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        resolve: {
          auth: function(sessionService) {
            return sessionService.isNotAuthenticated();
          }
        },
        templateUrl: 'app/login/login.html',
        controller: 'LoginController'
      })

      .state('main', {
        url: '/',
        abstract: true,
        templateUrl: 'app/main/main.html',
        controller: 'MainController'
      })

      // Operator section of site
      .state('main.emergencies', {
        url: 'emergencies',
        resolve: {
          auth: function(sessionService) {
            return sessionService.isAuthenticated('operator');
          }
        },
        views: {
          'left@main': {
            templateUrl: 'app/main/emergencies/emergencies-index/emergenciesIndex.html',
            controller: 'EmergenciesIndexController'
          },
          'right@main': {
            templateUrl: 'app/main/emergencies/no-emergencies-view/noEmergenciesView.html'
          }
        }
      })
      .state('main.emergencies.view', {
        url: '/{emergencyId:[0-9]+}',
        views: {
          'right@main': {
            templateUrl: 'app/main/emergencies/emergencies-view/emergenciesView.html',
            controller: 'EmergenciesViewController'
          }
        }
      })

      // Reporter section of site
      .state('main.report', {
        url: 'report',
        resolve: {
          auth: function(sessionService) {
            return sessionService.isAuthenticated('reporter');
          }
        },
        views: {
          'left@main': {
            templateUrl: 'app/main/emergencies/emergencies-create/emergenciesCreate.html',
            controller: 'EmergenciesCreateController'
          },
          'right@main': {
            templateUrl: 'app/main/emergencies/no-emergencies-view/noEmergenciesView.html'
          }
        }
      })
      .state('main.report.view', {
        url: '/{emergencyId:[0-9]+}',
        views: {
          'right@main': {
            templateUrl: 'app/main/emergencies/emergencies-view/emergenciesView.html',
            controller: 'EmergenciesViewController'
          }
        }
      });

    $urlRouterProvider.otherwise('/emergencies');
  }

})();
