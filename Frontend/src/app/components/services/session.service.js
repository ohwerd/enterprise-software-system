(function() {
  'use strict';

  angular
    .module('emergencyResponseService')
    .factory('sessionService', sessionService);

  /** @ngInject */
  function sessionService($http, $q, $timeout, $state, $cookieStore) {

    var service = {
      login: function(username, password) {
        var user = {
          username: username,
          password: password,
          role: username.indexOf("Operator") > -1 ? 'operator' : 'reporter'
        }
        service.setCurrentUser(user, password);
        redirectWhenAuthed(user.role);
      },

      logout: function() {
        service.setCurrentUser(null);
        redirectWhenNotAuthed();
      },

      getCurrentUser: function() {
        return $cookieStore.get('currentUser');
      },

      setCurrentUser: function(user) {
        $cookieStore.put('currentUser', user);
        if (user) {
          setHeaders(user.username, user.password);
        } else {
          clearHeaders();
          redirectWhenNotAuthed();
        }
      },

      isAuthenticated: function(role) {
        var user = service.getCurrentUser();

        if (user && (!role || user.role == role)) {
          return $q.when();
        } else {
          $timeout(function() {
            redirectWhenNotAuthed();
          });
          return $q.reject();
        }
      },

      isNotAuthenticated: function() {
        var user = service.getCurrentUser();

        if (!user) {
          return $q.when();
        } else {
          $timeout(function() {
            redirectWhenAuthed(user.role);
          });
          return $q.reject();
        }
      }
    };

    function setHeaders(username, password) {
      $http.defaults.headers.common['Authorization'] = genAuthHeader(username, password);
      console.log($http.defaults.headers.common['Authorization']);
    };

    function clearHeaders() {
      $http.defaults.headers.common['Authorization'] = null;
    }

    function genAuthHeader(username, password) {
      return "Basic " + btoa(username + ':' + password);
    };

    function redirectWhenAuthed(role) {
      if (role == 'operator') {
        $state.go('main.emergencies');
      } else if (role == 'reporter') {
        $state.go('main.report');
      }
    };

    function redirectWhenNotAuthed() {
      $state.go('login');
    };

    return service;
  }
})();