package com.swen90017.emergency.core;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

import java.util.Date;

/**
 * Created by edwardcrupi on 8/09/15.
 */
@Entity
@Table(name = "emergencies")
@NamedQueries({
        @NamedQuery(
                name = "com.swen90017.emergency.core.Emergency.findAll",
                query = "SELECT e FROM Emergency e"
        )
})
public class Emergency {

    @Id
    @GeneratedValue
    private long id;

    private long operatorId;

    @Column(nullable = false)
    private String reporterFirstName;

    @Column(nullable = false)
    private String reporterLastName;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String location;

    @Column(nullable = false)
    private boolean fireRequired;

    @Column(nullable = false)
    private boolean amboRequired;

    @Column(nullable = false)
    private boolean policeRequired;

    @Column(nullable = false)
    private boolean isOpen;

    @Column(nullable = false)
    private Date createdAt;

    @Column(nullable = false)
    private Date updatedAt;

    public Emergency(){};

    @JsonCreator
    public Emergency(@JsonProperty("operatorId") long operatorId,
                     @JsonProperty("reporterFirstName") String reporterFirstName,
                     @JsonProperty("reporterLastName") String reporterLastName,
                     @JsonProperty("description") String description,
                     @JsonProperty("location") String location,
                     @JsonProperty("fireRequired") boolean fireRequired,
                     @JsonProperty("amboRequired") boolean amboRequired,
                     @JsonProperty("policeRequired") boolean policeRequired,
                     @JsonProperty("isOpen") boolean isOpen) {
        this.operatorId = operatorId;
        this.reporterFirstName = reporterFirstName;
        this.reporterLastName = reporterLastName;
        this.description = description;
        this.location = location;
        this.fireRequired = fireRequired;
        this.amboRequired = amboRequired;
        this.policeRequired = policeRequired;
        this.isOpen = isOpen;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty("operatorId")
    public long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(long operatorId) {
        this.operatorId = operatorId;
    }

    @JsonProperty("reporterFirstName")
    public String getReporterFirstName() {
        return reporterFirstName;
    }

    public void setReporterFirstName(String reporterFirstName) {
        this.reporterFirstName = reporterFirstName;
    }

    @JsonProperty("reporterLastName")
    public String getReporterLastName() {
        return reporterLastName;
    }

    public void setReporterLastName(String reporterLastName) {
        this.reporterLastName = reporterLastName;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @JsonProperty("fireRequired")
    public boolean isFireRequired() {
        return fireRequired;
    }

    public void setFireRequired(boolean fireRequired) {
        this.fireRequired = fireRequired;
    }

    @JsonProperty("amboRequired")
    public boolean isAmboRequired() {
        return amboRequired;
    }

    public void setAmboRequired(boolean amboRequired) {
        this.amboRequired = amboRequired;
    }

    @JsonProperty("policeRequired")
    public boolean isPoliceRequired() {
        return policeRequired;
    }

    public void setPoliceRequired(boolean policeRequired) {
        this.policeRequired = policeRequired;
    }

    @JsonProperty("isOpen")
    public boolean isIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    @JsonProperty("createdAt")
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updatedAt")
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) { this.updatedAt = updatedAt; }
}
