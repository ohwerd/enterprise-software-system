package com.swen90017.emergency.core;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by edwardcrupi on 7/10/15.
 */
public class User {
    public static final String ROLE_OPERATOR = "operator";
    public static final String ROLE_REPORTER = "reporter";

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String role;

    public User(){};

    @JsonCreator
    private User(@JsonProperty("username") String username,
                 @JsonProperty("password") String password,
                 @JsonProperty("role") String role){
        this.username = username;
        this.password = password;
        this.role = role;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("role")
    public String getRole(){
        return role;
    }

    public void setRole(String role){
        this.role = role;
    }

}
