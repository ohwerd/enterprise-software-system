package com.swen90017.emergency.resources;

import com.swen90017.emergency.core.Emergency;
import com.swen90017.emergency.core.User;
import com.swen90017.emergency.db.EmergencyDAO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

/**
 * Created by edwardcrupi on 8/09/15.
 */
@Path("/emergencies")
@Produces(MediaType.APPLICATION_JSON)
public class EmergenciesResource {
    private final EmergencyDAO emergenciesDAO;

    public EmergenciesResource(EmergencyDAO emergenciesDAO){
        this.emergenciesDAO = emergenciesDAO;
    }

    @GET
    @UnitOfWork
    public Response listEmergencies(@Auth User user) {
        System.out.println("\n\n\n\n\nhowareya\n\n\n\n\n");
        System.out.println(user);
        if (user.getRole().equals(User.ROLE_REPORTER))
            return Response.status(Response.Status.FORBIDDEN).build();
        return Response.ok().entity(emergenciesDAO.findAll()).build();
    }

    @POST
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createEmergency(@Valid Emergency emergency,@Auth User user) {
        if (user.getRole().equals(User.ROLE_OPERATOR))
            return Response.status(Response.Status.FORBIDDEN).build();
        if(emergency != null) {
            emergency = (Emergency) emergenciesDAO.create(emergency);
        }
        return Response.created(UriBuilder
                                .fromResource(EmergencyResource.class)
                                .build(emergency))
                                .entity(emergency)
                                .build();
    }

    @Path("/merge/{e1Id}/{e2Id}")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @UnitOfWork
    public Response mergeEmergencies(@PathParam("e1Id") LongParam e1Id, @PathParam("e2Id") LongParam e2Id,@Auth User user){
        if (user.getRole().equals(User.ROLE_REPORTER))
            return Response.status(Response.Status.FORBIDDEN).build();
        Emergency emergency = emergenciesDAO.mergeEmergencies(e1Id.get(), e2Id.get());
        return Response.created(UriBuilder
                                .fromResource(EmergencyResource.class)
                                .build(emergency))
                                .entity(emergency)
                                .build();
    }

}
