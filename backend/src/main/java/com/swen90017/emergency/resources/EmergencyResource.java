package com.swen90017.emergency.resources;

import com.swen90017.emergency.core.Emergency;
import com.swen90017.emergency.core.User;
import com.swen90017.emergency.db.EmergencyDAO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

/**
 * Created by edwardcrupi on 8/09/15.
 */

@Path("/emergencies/{id}")
@Produces(MediaType.APPLICATION_JSON)
public class EmergencyResource {

    private final EmergencyDAO emergenciesDAO;

    public EmergencyResource(EmergencyDAO emergenciesDAO){
        this.emergenciesDAO = emergenciesDAO;
    }

    @GET
    @UnitOfWork
    public Emergency getEmergency(@PathParam("id") LongParam emergencyId, @Auth User user) {
        return emergenciesDAO.findById(emergencyId.get());
    }

    @PUT
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateEmergency(@PathParam("id") LongParam emergencyId, @Valid Emergency emergency,@Auth User user) {
        if(emergency != null && emergenciesDAO.findById(emergencyId.get()) != null) {
            emergency.setId(emergencyId.get());
            emergency = emergenciesDAO.updateEmergency(emergency);
            return Response.ok(UriBuilder.fromResource(EmergencyResource.class).build(emergency)).entity(emergency).build();
        }
        else return Response.status(Response.Status.NOT_FOUND).build();
    }

    @DELETE
    @UnitOfWork
    public Response deleteEmergency(@PathParam("id") LongParam emergencyId,@Auth User user) {
        if(emergenciesDAO.findById(emergencyId.get()) != null && user.getRole().equals(User.ROLE_REPORTER))
        {
            emergenciesDAO.deleteById(emergencyId.get());
            return Response.noContent().build();
        }
        else if (user.getRole().equals(User.ROLE_REPORTER)){
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        else
            return Response.status(Response.Status.NOT_FOUND).build();
    }
}
