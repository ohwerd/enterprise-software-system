package com.swen90017.emergency;


import com.google.common.base.Optional;
import com.swen90017.emergency.auth.SimpleAuthenticator;
import com.swen90017.emergency.core.Emergency;
import com.swen90017.emergency.core.User;
import com.swen90017.emergency.db.EmergencyDAO;
import com.swen90017.emergency.resources.EmergenciesResource;
import com.swen90017.emergency.resources.EmergencyResource;
import com.swen90017.emergency.resources.HelloWorldResource;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthFactory;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicAuthFactory;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.server.Authentication;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration.Dynamic;
import java.util.EnumSet;


public class ERSApplication extends Application<ERSConfiguration> {

    public static void main(String[] args) throws Exception {
        new ERSApplication().run(args);
    }

    private final HibernateBundle<ERSConfiguration> hibernate = new HibernateBundle<ERSConfiguration>(Emergency.class) {
        @Override
        public DataSourceFactory getDataSourceFactory(ERSConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    @Override
    public String getName() {
        return "hello-world";
    }

    @Override
    public void initialize(Bootstrap<ERSConfiguration> bootstrap) {
        bootstrap.addBundle(new MigrationsBundle<ERSConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(ERSConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(hibernate);
    }

    @Override
    public void run(ERSConfiguration configuration,
                Environment environment) {

        // add cors headers to responses
        configureCors(environment);

        // Adds security provider so resource methods decorated with auth attribute will use this authenticator
        environment.jersey().register(AuthFactory.binder(new BasicAuthFactory<User>(new SimpleAuthenticator(),
                "SUPER SECRET STUFF",
                User.class)));

        //Registration of the hellowworld resource
        final HelloWorldResource resource = new HelloWorldResource(
            configuration.getTemplate(),
            configuration.getDefaultName()
        );
        environment.jersey().register(resource);

        // Instantiation of the emergencies DAO to pass to emergency and emergencies resources
        final EmergencyDAO emergenciesDao = new EmergencyDAO(hibernate.getSessionFactory());

        //Registration of the emergency and emergencies resources
        environment.jersey().register(new EmergencyResource(emergenciesDao));
        environment.jersey().register(new EmergenciesResource(emergenciesDao));

    }

    private void configureCors(Environment environment) {
        Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        filter.setInitParameter("allowCredentials", "true");
    }

}