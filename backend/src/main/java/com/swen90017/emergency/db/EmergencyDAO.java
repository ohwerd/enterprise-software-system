package com.swen90017.emergency.db;

import com.swen90017.emergency.core.Emergency;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.Date;
import java.util.List;

/**
 * Created by edwardcrupi on 8/09/15.
 */
public class EmergencyDAO extends AbstractDAO<Emergency> {

    public EmergencyDAO(SessionFactory factory) {
            super(factory);
        }

    public Emergency findById(Long id) {
        return get(id);
    }

    public Emergency create(Emergency emergency) {
        Date time = new Date();
        emergency.setCreatedAt(time);
        emergency.setUpdatedAt(time);
        emergency.setIsOpen(true);
        return persist(emergency);
    }

    public Emergency updateEmergency(Emergency emergency) {
        Emergency newEmergency = get(emergency.getId());

        newEmergency.setOperatorId(emergency.getOperatorId());
        newEmergency.setDescription(emergency.getDescription());
        newEmergency.setReporterLastName(emergency.getReporterLastName());
        newEmergency.setReporterFirstName(emergency.getReporterFirstName());
        newEmergency.setAmboRequired(emergency.isAmboRequired());
        newEmergency.setPoliceRequired(emergency.isPoliceRequired());
        newEmergency.setFireRequired(emergency.isFireRequired());
        newEmergency.setLocation(emergency.getLocation());
        newEmergency.setIsOpen(emergency.isIsOpen());

        Date time = new Date();
        newEmergency.setUpdatedAt(time);
        return persist(newEmergency);
    }

    public Emergency mergeEmergencies(Long e1Id, Long e2Id){
        Emergency e1 = get(e1Id);
        Emergency e2 = get(e2Id);
        Emergency newEmergency = new Emergency();
        if (e1.getCreatedAt().before(e2.getCreatedAt()) || (e1.getCreatedAt().equals(e2.getCreatedAt()))){
            newEmergency = e1;
            newEmergency.setDescription(e1.getDescription()+ "\n At "+
                                        e2.getCreatedAt().toString()+ "\n "+
                                        e2.getReporterFirstName()+" "+
                                        e2.getReporterLastName()+" said:\n"+
                                        e2.getDescription()+"\n in"+
                                        e2.getLocation());
        } else {
            newEmergency = e2;
            newEmergency.setDescription(e1.getDescription()+ "\n At "+
                    e1.getCreatedAt().toString()+ "\n "+
                    e1.getReporterFirstName()+" "+
                    e1.getReporterLastName()+" said:\n"+
                    e1.getDescription()+"\n in"+
                    e1.getLocation());
        }
        deleteById(e1.getId());
        deleteById(e2.getId());
        return persist(newEmergency);
    }
    public List<Emergency> findAll() {
        return list(namedQuery("com.swen90017.emergency.core.Emergency.findAll"));
    }

    public void deleteById(Long id){
        Emergency emergency = get(id);
        currentSession().delete(emergency);
    }
}
