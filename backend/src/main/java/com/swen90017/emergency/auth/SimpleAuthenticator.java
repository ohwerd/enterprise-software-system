package com.swen90017.emergency.auth;

import com.swen90017.emergency.core.User;
import io.dropwizard.auth.AuthenticationException;

import com.google.common.base.Optional;
import io.dropwizard.auth.Authenticator;

/**
 * Created by edwardcrupi on 7/10/15.
 */

public class SimpleAuthenticator implements Authenticator<io.dropwizard.auth.basic.BasicCredentials, User> {

    @Override
    public Optional<User> authenticate(io.dropwizard.auth.basic.BasicCredentials basicCredentials) throws AuthenticationException {
        // This is where you should call your authentication service and validate the token
        System.out.println("\n\n\n\nhellloooo\n\n\n");
        if (basicCredentials.getPassword().equals("password")) {
            System.out.println("bad password was: " +
                                basicCredentials.getPassword() +
                                " and that is " + basicCredentials.getPassword().matches("password"));
            return Optional.absent();
            // throw new AuthenticationException("Invalid credentials");
        } else {
            User user = new User();
            user.setUsername("User for password" + basicCredentials.getPassword());
            user.setUsername(basicCredentials.getPassword());
            user.setRole(basicCredentials.getUsername().contains("Operator") ? User.ROLE_OPERATOR : User.ROLE_REPORTER);

            return Optional.fromNullable(user);
        }
    }
}
