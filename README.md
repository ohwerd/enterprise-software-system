# README #

This is the README for the SWEN90017 Software Design and Architecture class at The University of Melbourne. This README is and should be used document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for the creation of the Enterprise Software System developed as part of the SWEN90017 Software Design and Architecture class. The frontend is to be developed in Angular whilst the backend is using the dropwizard java framework.

Version 0.0

### How do I get set up? ###

* Summary of set up
	- We have split the project up in to a frontend and a backend. The frontednd uses AngularJS and while the backend uses Java's Dropwizard.
* Dependencies
	- Dropwizard is already dependant on Maven's Shade plugin as well as Maven's Jar plugin
* Database configuration
	- The database could be Derby, MySQL or Postgres... we haven't decided yet.
* How to run tests
	- We have none
* Deployment instructions
	- Make sure you have a PostgreSQL database named ```emergency_development``` and a role named ```emergency```, no password needed.
	- To deploy the backend server make sure you have a working copy of maven installed and run:
	```
		mvn package
		java -jar target/my-app-1.0-SNAPSHOT.jar server hello-world.yml
	```

### Who do I talk to? ###

[Ed Crupi](https://bitbucket.org/ohwerd/ "Ed") or [Jordan Steele](https://bitbucket.org/jordan_steele/ "Jordan")